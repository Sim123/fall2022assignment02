package Application;
import vehicles.Bicycle;

/**
 *	BikeStore is a class that uses the object class Bicycle in the package vehicles
 *  @author   Simona Gragomir
 *  @version  9/01/2022
*/

public class BikeStore {
    public static void main(String args[]) {
        Bicycle[] obj = new Bicycle[4];
        obj[0] = new Bicycle("ABC", 18, 30);
        obj[1] = new Bicycle("Trigger", 21, 40);
        obj[2] = new Bicycle("Ace Bikes", 24, 50);
        obj[3] = new Bicycle("Fantasy Bikes Fake Store", 27, 70);

        for (int i = 0; i < obj.length; i++) {
            System.out.println(obj[i].toString());
        }
    }
}
