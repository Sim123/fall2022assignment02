package vehicles;

/**
 *	Bicycle is a class used for creating an object Bicycle
 *  @author   Simona Gragomir
 *  @version  9/01/2022
*/

public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manu, int gears, double speed) {
        this.manufacturer = manu;
        this.numberGears = gears;
        this.maxSpeed = speed;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public int getNumberGears() {
        return this.numberGears;
    }

    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    public String toString() {
        String s = "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed;
        return s;
    }

}